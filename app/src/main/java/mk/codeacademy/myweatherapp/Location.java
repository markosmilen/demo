package mk.codeacademy.myweatherapp;

public class Location {

    String city;
    String state;
    String temperature;
    String humidity;
    String time;
    int weatherPic;

    public Location (){}

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getWeatherPic() {
        return weatherPic;
    }

    public void setWeatherPic(int weatherPic) {
        this.weatherPic = weatherPic;
    }
}
