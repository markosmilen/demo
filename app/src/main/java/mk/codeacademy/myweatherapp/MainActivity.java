package mk.codeacademy.myweatherapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Location> locations = new ArrayList<>();
    RecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        getLocation();
        adapter = new RecyclerViewAdapter(this, locations);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    private void getLocation(){
        Location city1 = new Location();
        city1.setCity("San Diego");
        city1.setState("Current Location");
        city1.setTemperature("77°");
        city1.setHumidity("0%");
        city1.setTime("3:48PM");
        city1.setWeatherPic(R.drawable.sunny_android);

        Location city2 = new Location();
        city2.setCity("Austin");
        city2.setState("Texas");
        city2.setTemperature("97°");
        city2.setHumidity("0%");
        city2.setTime("5:48PM");
        city2.setWeatherPic(R.drawable.sunny_android);

        Location city3 = new Location();
        city3.setCity("New York");
        city3.setState("New York");
        city3.setTemperature("87°");
        city3.setHumidity("0%");
        city3.setTime("6:48PM");
        city3.setWeatherPic(R.drawable.sunny_android);

        Location city4 = new Location();
        city4.setCity("Skopje");
        city4.setState("Macedonia");
        city4.setTemperature("57°");
        city4.setHumidity("0%");
        city4.setTime("21:48PM");
        city4.setWeatherPic(R.drawable.sunny_android);
    }
}
