package mk.codeacademy.myweatherapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.onViewHolder>{
    List<Location> data;
    LayoutInflater inflater;

    public RecyclerViewAdapter(Context context, List<Location> data) {
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public onViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.weather_items, parent, false);
        return new onViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull onViewHolder holder, int position) {
        Location locate = data.get(position);
        holder.cityId.setText(locate.getCity());
        holder.state.setText(locate.getState());
        holder.temperature.setText(locate.temperature);
        holder.humidity.setText(locate.humidity);
        holder.time.setText(locate.time);
        holder.weather.setImageResource(locate.weatherPic);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class onViewHolder extends RecyclerView.ViewHolder {
        TextView cityId;
        TextView state;
        TextView time;
        TextView temperature;
        TextView humidity;
        ImageView weather;
        public onViewHolder(@NonNull View itemView) {

            super(itemView);
            cityId = (TextView) itemView.findViewById(R.id.cityID);
            state = (TextView) itemView.findViewById(R.id.stateID);
            time = (TextView) itemView.findViewById(R.id.time);
            temperature = (TextView) itemView.findViewById(R.id.temperature);
            humidity = (TextView) itemView.findViewById(R.id.humidity);
            weather = (ImageView) itemView.findViewById(R.id.weatherPic);
        }
    }
}
